# coding=utf-8
from __future__ import absolute_import
from distutils.version import LooseVersion

import requests
import octoprint.plugin
import os.path
from subprocess import call


class HubSetup(octoprint.plugin.StartupPlugin,):

    # STARTUPPLUGIN
    def on_after_startup(self):
        if os.path.isdir("/home/pi/OctoPrint/venv/lib/python2.7/site-packages/Canvas-0.1.0-py2.7.egg-info/") and os.path.isdir("/home/pi/.mosaicdata/turquoise/"):
            call(
                ["sudo chown -R pi:pi /home/pi/OctoPrint/venv/lib/python2.7/site-packages/"], shell=True)


# If you want your plugin to be registered within OctoPrint under a different name than what you defined in setup.py
# ("OctoPrint-PluginSkeleton"), you may define that here. Same goes for the other metadata derived from setup.py that
# can be overwritten via __plugin_xyz__ control properties. See the documentation for that.
__plugin_name__ = "Hub Setup"
__plugin_description__ = "A plugin for the initial Canvas Hub Setup"


def __plugin_load__():
    global __plugin_implementation__
    __plugin_implementation__ = HubSetup()
